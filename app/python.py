from flask import Flask 
from flask import render_template

app = Flask(__name__)

if __name__ == "__main__":
   app.run(host="0.0.0.0", port="5000", debug= True)
 
@app.route('/')
def hello_world():
   return 'Hola mundo'

@app.route('/ajax.html')
def about():
   return render_template("ajax.html")


@app.route('/user/<userid>')
def get_user(userid):
   return userid